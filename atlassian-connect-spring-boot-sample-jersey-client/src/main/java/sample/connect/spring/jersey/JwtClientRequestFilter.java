package sample.connect.spring.jersey;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import java.io.IOException;

@Component
public class JwtClientRequestFilter implements ClientRequestFilter {

    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;

    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {
        HttpMethod method = HttpMethod.resolve(requestContext.getMethod());
        String jwt = atlassianHostRestClients.createJwt(method, requestContext.getUri());
        requestContext.getHeaders().add("Authorization", String.format("JWT %s", jwt));
    }
}
