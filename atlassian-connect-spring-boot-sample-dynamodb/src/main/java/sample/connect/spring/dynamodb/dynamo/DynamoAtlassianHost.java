package sample.connect.spring.dynamodb.dynamo;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIndexHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.atlassian.connect.spring.AtlassianHost;
import org.springframework.beans.BeanUtils;

import java.util.Calendar;

/**
 * DynamoDB equivalent of {@link AtlassianHost}.
 *
 * Index: baseUrl.
 */
@DynamoDBTable(tableName = DynamoAtlassianHost.TABLE_NAME)
public class DynamoAtlassianHost extends AtlassianHost {

    static final String TABLE_NAME = "atlassian-host";

    static DynamoAtlassianHost fromAtlassianHost(AtlassianHost atlassianHost) {
        DynamoAtlassianHost dynamoAtlassianHost = new DynamoAtlassianHost();
        BeanUtils.copyProperties(atlassianHost, dynamoAtlassianHost);
        return dynamoAtlassianHost;
    }

    public DynamoAtlassianHost() {
    }

    @Override
    @DynamoDBHashKey
    public String getClientKey() {
        return super.getClientKey();
    }

    @Override
    @DynamoDBAttribute
    public String getPublicKey() {
        return super.getPublicKey();
    }

    @Override
    @DynamoDBAttribute
    public String getOauthClientId() {
        return super.getOauthClientId();
    }

    @Override
    @DynamoDBAttribute
    public String getSharedSecret() {
        return super.getSharedSecret();
    }

    @Override
    @DynamoDBIndexHashKey(globalSecondaryIndexName = "base-url-index")
    public String getBaseUrl() {
        return super.getBaseUrl();
    }

    @Override
    @DynamoDBAttribute
    public String getProductType() {
        return super.getProductType();
    }

    @Override
    @DynamoDBAttribute
    public String getDescription() {
        return super.getDescription();
    }

    @Override
    @DynamoDBAttribute
    public String getServiceEntitlementNumber() {
        return super.getServiceEntitlementNumber();
    }

    @Override
    @DynamoDBAttribute
    public boolean isAddonInstalled() {
        return super.isAddonInstalled();
    }

    @Override
    @DynamoDBAttribute
    public Calendar getCreatedDate() {
        return super.getCreatedDate();
    }

    @Override
    @DynamoDBAttribute
    public Calendar getLastModifiedDate() {
        return super.getLastModifiedDate();
    }

    @Override
    @DynamoDBAttribute
    public String getCreatedBy() {
        return super.getCreatedBy();
    }

    @Override
    @DynamoDBAttribute
    public String getLastModifiedBy() {
        return super.getLastModifiedBy();
    }

    @Override
    public String toString() {
        return "Dynamo" + super.toString();
    }
}
