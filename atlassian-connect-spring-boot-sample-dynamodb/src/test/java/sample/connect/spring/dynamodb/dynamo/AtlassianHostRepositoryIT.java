package sample.connect.spring.dynamodb.dynamo;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static java.util.Optional.of;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AtlassianHostRepositoryIT {

    @Autowired
    private AtlassianHostRepository hostRepository;

    private AtlassianHost host;

    @Before
    public void setUp() {
        hostRepository.deleteAll();
        host = createAndSaveHost();
    }

    private AtlassianHost createAndSaveHost() {
        final AtlassianHost atlassianHost = new AtlassianHost();
        atlassianHost.setClientKey(randomAlphanumeric(32));
        atlassianHost.setBaseUrl("http://localhost");
        return hostRepository.save(atlassianHost);
    }

    @Test
    public void shouldInjectDynamoImplementation() {
        assertThat(hostRepository, instanceOf(DynamoAtlassianHostRepository.class));
    }

    @Test
    public void shouldHostRepositoryHaveOneHost() {
        assertThat(hostRepository.count(), is(1L));
    }

    @Test
    public void shouldFindHostByBaseUrl() {
        assertThat(hostRepository.findFirstByBaseUrl(host.getBaseUrl()), is(of(host)));
    }

    @Test
    public void shouldFindHostByClientKey() {
        assertThat(hostRepository.findById(host.getClientKey()).orElse(null), equalTo(host));
    }
}
