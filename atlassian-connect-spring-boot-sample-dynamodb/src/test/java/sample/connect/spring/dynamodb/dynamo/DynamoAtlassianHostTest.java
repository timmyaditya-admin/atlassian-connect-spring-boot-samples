package sample.connect.spring.dynamodb.dynamo;

import com.atlassian.connect.spring.AtlassianHost;
import org.junit.Test;

import java.util.Calendar;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class DynamoAtlassianHostTest {

    @Test
    public void shouldCreateDynamoAtlassianHostFromAtlassianHost() {
        // given:
        final AtlassianHost host = new AtlassianHost();
        host.setClientKey("ClientKey");
        host.setPublicKey("PublicKey");
        host.setOauthClientId("OauthClientId");
        host.setSharedSecret("SharedSecret");
        host.setBaseUrl("BaseUrl");
        host.setProductType("ProductType");
        host.setDescription("Description");
        host.setServiceEntitlementNumber("ServiceEntitlementNumber");
        host.setAddonInstalled(true);
        host.setCreatedDate(Calendar.getInstance());
        host.setLastModifiedDate(Calendar.getInstance());
        host.setCreatedBy("CreatedBy");
        host.setLastModifiedBy("LastModifiedBy");

        // when:
        final DynamoAtlassianHost dynamoHost = DynamoAtlassianHost.fromAtlassianHost(host);

        // then:
        assertThat(dynamoHost, allOf(
            hasProperty("clientKey", equalTo(host.getClientKey())),
            hasProperty("publicKey", equalTo(host.getPublicKey())),
            hasProperty("oauthClientId", equalTo(host.getOauthClientId())),
            hasProperty("sharedSecret", equalTo(host.getSharedSecret())),
            hasProperty("baseUrl", equalTo(host.getBaseUrl())),
            hasProperty("productType", equalTo(host.getProductType())),
            hasProperty("description", equalTo(host.getDescription())),
            hasProperty("serviceEntitlementNumber", equalTo(host.getServiceEntitlementNumber())),
            hasProperty("addonInstalled", equalTo(host.isAddonInstalled())),
            hasProperty("createdDate",equalTo(host.getCreatedDate())),
            hasProperty("lastModifiedDate", equalTo(host.getLastModifiedDate())),
            hasProperty("createdBy", equalTo(host.getCreatedBy())),
            hasProperty("lastModifiedBy", equalTo(host.getLastModifiedBy()))
        ));
    }
}
