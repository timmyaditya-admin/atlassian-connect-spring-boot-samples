# DynamoDB Sample
This sample demonstrates how to store atlassian-host data inside DynamoDB.

## Demo
Create a public tunnel to your add-on service using: 

```
ngrok http 8080
[...]
Forwarding                    https://55c07c86.ngrok.io -> localhost:8080
[...]
```

Start the add-on service:
```
mvn spring-boot:run -Drun.arguments="--addon.base-url=https://55c07c86.ngrok.io,--amazon.dynamodb.sqlite.file=embedded-dynamo.db"
```

Install addon using ngrok URL (https://55c07c86.ngrok.io in this case) at your Jira instance 
(let's say https://your-instance.atlassian.net).

Make sure that a new entry was added to atlassian-host DynamoDB table:
```
sqlite3 embedded-dynamo.db "select * from 'atlassian-host'"
```

Make a curl call to the add-on service:
```
curl http://localhost:8080/myself?hostBaseUrl=https://your-instance.atlassian.net
```
